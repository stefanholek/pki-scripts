#!/bin/bash
#
# Functions shared by pki commands
#

if [ "$MARKER" ]; then
    return
else
    MARKER="--marker--"
fi

if [[ $OSTYPE =~ darwin ]]; then
    DARWIN=true
else
    DARWIN=
fi

if date -d @12345 >/dev/null 2>&1; then
    GNU=true
else
    GNU=
fi

openssl=${OPENSSL:-openssl}

function msg_exit() {
    echo "$@"; exit 0
}

function err_exit() {
    echo "$@" >&2; exit 1
}

function expect() {
    eval $1'="$MARKER"'
}

function assign() {
    if [[ $2 == -* && $3 != nocheck ]]; then usage; fi
    eval $1'="$2"'
}

function append() {
    if [[ $2 == -* && $3 != nocheck ]]; then usage; fi
    eval $1'+=("$2")'
}

function ask_yesno() {
    while true; do
        read -p "$@ (y/n)" -n 1
        echo
        [[ $REPLY == y ]] && return 0
        [[ $REPLY == n ]] && return 1
    done
}

function ask_yesno_like_openssl_ca() {
    read -p "$@ [y/n]:"
    if [[ $REPLY == y ]]; then
        return 0
    else
        return 1
    fi
}

function to_lower() {
    echo "$@" | tr '[:upper:]' '[:lower:]'
}

function to_upper() {
    echo "$@" | tr '[:lower:]' '[:upper:]'
}

function join() {
    # https://code-examples.net/en/q/174d09
    local d=$1 s=$2; shift 2 && printf %s "$s${@/#/$d}"
}

function index_of() {
    local match="$1"
    local i=0

    for (( i=2; i <= $#; i++ )); do
        if [[ ${!i} == $match ]]; then
            echo $((i-2))
            return
        fi
    done
    echo -1
}

function ensure_suffix() {
    if [[ $1 == *$2 ]]; then
       echo "$1"
    else
       echo "$1$2"
    fi
}

function is_pem_file() {
    command grep -E -q '^-----BEGIN .*-----' -- "$1"
}

function is_pem_multi() {
    local count=$(command grep -E -c '^-----BEGIN .*-----' -- "$1")

    if [[ $count -ge 2 ]]; then
        return 0
    else
        return 1
    fi
}

function get_inform() {
    if is_pem_file "$1"; then
        echo PEM
    else
        echo DER
    fi
}

function pem_filter() {
    local line=""
    local in_pem_block=
    local IFS=$'\n'

    while read -r line; do
        if [[ ! $in_pem_block ]]; then
            if [[ $line == '-----BEGIN '* ]]; then
                in_pem_block=true
                echo "$line"
            fi
        else
            echo "$line"
            if [[ $line == '-----END '* ]]; then
                in_pem_block=
            fi
        fi
    done
}

function config_sections() {
    local file="$1"
    local suffix="$2"
    local line=""
    local IFS=$'\n'

    while read line; do
        if [[ $line =~ ^"["[[:space:]]*([[:alnum:]_]*$suffix)[[:space:]]*"]" ]]; then
            echo "${BASH_REMATCH[1]}"
        fi
    done < "$file"
}

function config_keys() {
    local file="$1"
    local section="$2"
    local line=""
    local in_section=
    local IFS=$'\n'

    while read line; do
        if [[ ! $in_section ]]; then
            if [[ $line =~ ^"["[[:space:]]*${section}[[:space:]]*"]" ]]; then
                in_section=true
            fi
        else
            if [[ $line == "["* ]]; then
                return
            elif [[ $line =~ ^([[:alnum:]_]+)[[:space:]]*= ]]; then
                echo "${BASH_REMATCH[1]}"
            fi
        fi
    done < "$file"
}

function config_value() {
    local file="$1"
    local section="$2"
    local key="$3"
    local line=""
    local in_section=
    local IFS=$'\n'

    while read line; do
        if [[ ! $in_section ]]; then
            if [[ $line =~ ^"["[[:space:]]*${section}[[:space:]]*"]" ]]; then
                in_section=true
            fi
        else
            if [[ $line == "["* ]]; then
                return
            elif [[ $line =~ ^${key}[[:space:]]*=[[:space:]]*(.*) ]]; then
                line=${BASH_REMATCH[1]%%#*}
                line=$(IFS=$' \t\n'; echo $line)
                echo "$line"
                return
            fi
        fi
    done < "$file"
}

function check_read() {
    local file="${1%/}"
    local cmd="exit"

    if [ "$2" = "noexit" ]; then
        cmd="return"
    fi
    if [ ! -e "$file" ]; then
        echo "$SCRIPT: $file: No such file" >&2
        eval $cmd 1
    fi
    if [ ! -f "$file" ]; then
        echo "$SCRIPT: $file: Not a file" >&2
        eval $cmd 1
    fi
    if [ ! -r "$file" ]; then
        echo "$SCRIPT: $file: No read access" >&2
        eval $cmd 1
    fi
    if [ ! -s "$file" ]; then
        echo "$SCRIPT: $file: File is empty" >&2
        eval $cmd 1
    fi
    return 0
}

function check_pem() {
    local file="${1%/}"
    local cmd="exit"

    if [ "$2" = "noexit" ]; then
        cmd="return"
    fi
    if [ "$(get_inform "$file")" != "PEM" ]; then
        echo "$SCRIPT: $file: PEM format expected" >&2
        eval $cmd 1
    fi
    return 0
}


#!/bin/bash
#
# This script creates an EC private key
#
SCRIPT="pki $(basename "$0")"
SCRIPTDIR=$(dirname "$0")
KEYOUT=""
CURVE=""
PARAMFILE=""
CIPHER=""

source "$SCRIPTDIR/libpki.sh"

function usage() {
    err_exit "Usage: $SCRIPT -out filename [-paramfile filename] [-name curve] [-cipher alg]"
}

function parse_args() {
    local arg=""
    for arg in "$@"; do
        if [ "$KEYOUT" = "$MARKER" ]; then
            assign KEYOUT "$arg"
        elif [ "$CURVE" = "$MARKER" ]; then
            assign CURVE "$arg"
        elif [ "$PARAMFILE" = "$MARKER" ]; then
            assign PARAMFILE "$arg"
        elif [ "$CIPHER" = "$MARKER" ]; then
            assign CIPHER "$arg"
        elif [ "$arg" = "-o" -o "$arg" = "-out" ]; then
            expect KEYOUT
        elif [ "$arg" = "-n" -o "$arg" = "-name" ]; then
            expect CURVE
        elif [ "$arg" = "-p" -o "$arg" = "-paramfile" ]; then
            expect PARAMFILE
        elif [ "$arg" = "-c" -o "$arg" = "-cipher" ]; then
            expect CIPHER
        elif [ "$arg" = "-h" -o "$arg" = "-help" ]; then
            usage
        elif [ "$arg" = "-printopts" ]; then
            msg_exit "-out -name -paramfile -cipher -help"
        else
            usage
        fi
    done
    for arg in KEYOUT CURVE PARAMFILE CIPHER; do
        if [ "${!arg}" = "$MARKER" ]; then usage; fi
    done
    if [ ! "$KEYOUT" ]; then
        err_exit "$SCRIPT: -out option required"
    fi
    if [ ! "$CURVE" -a ! "$PARAMFILE" ]; then
        err_exit "$SCRIPT: -paramfile or -name option required"
    fi
    if [ "$CURVE" -a "$PARAMFILE" ]; then
        err_exit "$SCRIPT: Only one of -paramfile and -name allowed"
    fi
}

# Main
parse_args "$@"

if [ -f "$KEYOUT" ]; then
    ask_yesno "Key exists, overwrite?"
    if [ $? -eq 1 ]; then
        err_exit "Keeping existing key"
    fi
fi

if [ "$CIPHER" ]; then
    CIPHER="-$CIPHER"
fi

if [ "$PARAMFILE" ]; then
    $openssl genpkey \
        -out "$KEYOUT" \
        -paramfile "$PARAMFILE" $CIPHER
else
    $openssl genpkey \
        -out "$KEYOUT" \
        -algorithm ec \
        -pkeyopt ec_paramgen_curve:$CURVE $CIPHER
fi


#
# Bash completion for pki 2022.8
#
# Copyright (C) 2020-2022, Stefan H. Holek <stefan@epy.co.at>
# Apache License Version 2.0
#
# Completes pki command names and their options and arguments.
#
# Still works with Bash 3.2 (macOS). Also works with Zsh if compinit
# and bashcompinit have been loaded.
#
# To activate add this to your ~/.bashrc or ~/.zshrc:
#
#   alias pki='/path/to/pki-scripts/pki'
#   source '/path/to/pki-scripts/pki-completion.bash'
#

if [[ $BASH ]]; then
    shopt -s extglob
fi

if [[ $ZSH_VERSION ]]; then
    setopt EXTENDED_GLOB
    setopt BASH_REMATCH
fi

function _pki() {
    local self="$1"
    local cur="$2"
    local prev="$3"
    local cmd=""

    local crlreasons="unspecified superseded affiliationChanged \
                      keyCompromise CACompromise cessationOfOperation"

    local rsabits="2048 3072 4096 8192"
    local ecurves="P-256 P-384 P-521"
    local cipheralgs="des3 aes128 aes192 aes256"
    local filetypes="key req x509 crl pkcs7 pkcs12 pem"

    if [[ $ZSH_VERSION && $cur == -- ]]; then
        cur=""
    fi

    if [[ $COMP_CWORD -eq 1 ]]; then
        if [[ $cur == -* ]]; then
            _pki_wordlist "$(eval "$self" -printopts 2>/dev/null)" "$cur"
        else
            _pki_wordlist "$(eval "$self" -printcmds 2>/dev/null)" "$cur"
        fi
    else
        cmd="${COMP_WORDS[1]}"
        case $cmd in
            db)
                case $prev in
                    -c|-config)
                        _pki_filenames "!*.conf" "$cur"
                        return
                        ;;
                    -s|-serial)
                        _pki_opt_nodefault
                        return
                        ;;
                    -n|-crlnumber)
                        _pki_opt_nodefault
                        return
                        ;;
                esac
                ;;
            req)
                case $prev in
                    -c|-config)
                        _pki_filenames "!*.conf" "$cur"
                        return
                        ;;
                    -o|-out)
                        _pki_filenames "!*.csr" "$cur"
                        return
                        ;;
                    -k|-key|-i|-inkey|-t|-keyout)
                        _pki_filenames "!*.key" "$cur"
                        return
                        ;;
                    -e|-extensions)
                        _pki_sections "_reqext" "$cur"
                        return
                        ;;
                esac
                ;;
            sign)
                case $prev in
                    -c|-config)
                        _pki_filenames "!*.conf" "$cur"
                        return
                        ;;
                    -i|-in)
                        _pki_filenames "!*.csr" "$cur"
                        return
                        ;;
                    -o|-out)
                        _pki_filenames "!*.crt" "$cur"
                        return
                        ;;
                    -p|-policy)
                        _pki_sections "_pol" "$cur"
                        return
                        ;;
                    -e|-extensions)
                        _pki_sections "_ext" "$cur"
                        return
                        ;;
                    -d|-days)
                        _pki_opt_nodefault
                        return
                        ;;
                esac
                ;;
            revoke)
                case $prev in
                    -c|-config)
                        _pki_filenames "!*.conf" "$cur"
                        return
                        ;;
                    -i|-in)
                        _pki_filenames "!*.@(crt|pem)" "$cur"
                        return
                        ;;
                    -r|-reason)
                        _pki_wordlist "$crlreasons" "$cur"
                        return
                        ;;
                    -k|-keycomp|-a|-cacomp)
                        _pki_opt_nodefault
                        return
                        ;;
                    -s|-serial)
                        _pki_opt_nodefault
                        return
                        ;;
                esac
                ;;
            crl)
                case $prev in
                    -c|-config)
                        _pki_filenames "!*.conf" "$cur"
                        return
                        ;;
                    -o|-out)
                        _pki_filenames "!*.crl" "$cur"
                        return
                        ;;
                    -e|-extensions)
                        _pki_sections "_ext" "$cur"
                        return
                        ;;
                    -d|-days)
                        _pki_opt_nodefault
                        return
                        ;;
                esac
                ;;
            expire)
                case $prev in
                    -c|-config)
                        _pki_filenames "!*.conf" "$cur"
                        return
                        ;;
                esac
                ;;
            query)
                case $prev in
                    -c|-config)
                        _pki_filenames "!*.conf" "$cur"
                        return
                        ;;
                    -f|-fields)
                        _pki_opt_nodefault
                        return
                        ;;
                    -e)
                        _pki_opt_nodefault
                        return
                        ;;
                esac
                ;;
            rsa)
                case $prev in
                    -o|-out)
                        _pki_filenames "!*.key" "$cur"
                        return
                        ;;
                    -b|-bits)
                        _pki_wordlist "$rsabits" "$cur"
                        return
                        ;;
                    -c|-cipher)
                        _pki_wordlist "$cipheralgs" "$cur"
                        return
                        ;;
                esac
                ;;
            ec)
                case $prev in
                    -o|-out)
                        _pki_filenames "!*.key" "$cur"
                        return
                        ;;
                    -p|-paramfile)
                        _pki_filenames "!*.conf" "$cur"
                        return
                        ;;
                    -n|-name)
                        _pki_wordlist "$ecurves" "$cur"
                        return
                        ;;
                    -c|-cipher)
                        _pki_wordlist "$cipheralgs" "$cur"
                        return
                        ;;
                esac
                ;;
            pkcs7)
                case $prev in
                    -o|-out)
                        _pki_filenames "!*.@(p7c|p7b|p7r|spc)" "$cur"
                        return
                        ;;
                    -c|-certfile)
                        _pki_filenames "!*.@(crt|pem)" "$cur"
                        return
                        ;;
                esac
                ;;
            pkcs12)
                case $prev in
                    -i|-in)
                        _pki_filenames "!*.@(crt|pem)" "$cur"
                        return
                        ;;
                    -k|-key|-inkey)
                        _pki_filenames "!*.key" "$cur"
                        return
                        ;;
                    -o|-out)
                        _pki_filenames "!*.@(p12|pfx)" "$cur"
                        return
                        ;;
                    -c|-certfile)
                        _pki_filenames "!*.@(crt|pem)" "$cur"
                        return
                        ;;
                    -n|name|-a|-caname)
                        _pki_opt_nodefault
                        return
                        ;;
                esac
                ;;
            view)
                case $prev in
                    -t|-type)
                        _pki_wordlist "$filetypes" "$cur"
                        return
                        ;;
                esac
                ;;
        esac

        if [[ $cur == -* ]]; then
            _pki_wordlist "$(eval "$self" $cmd -printopts 2>/dev/null)" "$cur"
        else
            case $cmd in
                view|asn1)
                    _pki_filenames "!*" "$cur"
                    ;;
                enddate|hash|serial)
                    _pki_filenames "!*.crt" "$cur"
                    ;;
            esac
        fi
    fi
}

function _pki_bash3() {
    [[ ${BASH_VERSINFO[0]:-5} -le 3 ]]
}

function _pki_bash4() {
    [[ ${BASH_VERSINFO[0]:-0} -ge 4 ]]
}

function _pki_get_config() {
    local i=0

    for (( i=2; i < $COMP_CWORD; i++ )); do
        if [[ ${COMP_WORDS[i]} == @(-c|-config) ]]; then
            printf "%s\n" "${COMP_WORDS[i+1]}"
            return
        fi
    done
}

function _pki_get_sections() {
    local config="$1"
    local suffix="$2"
    local line=""
    local IFS=$'\n'

    while read line; do
        if [[ $line =~ ^"["[[:space:]]*([[:alnum:]_]*$suffix)[[:space:]]*"]" ]]; then
            printf "%s\n" "${BASH_REMATCH[1]%$suffix}"
        fi
    done < "$config"
}

function _pki_config_sections() {
    local suffix="$1"
    local config="$(_pki_get_config)"

    if [[ -f "$config" ]]; then
        _pki_get_sections "$config" "$suffix"
    fi
}

function _pki_file_filter() {
    local line=""
    local IFS=$'\n'

    while read -r line; do
        if [[ ! -d "$line" ]]; then
            printf "%s\n" "$line"
        fi
    done
}

function _pki_dir_filter() {
    local line=""
    local IFS=$'\n'

    while read -r line; do
        if [[ -d "$line" ]]; then
            if _pki_bash3; then
                printf "%s/\n" "$line"
            else
                printf "%s\n" "$line"
            fi
        fi
    done
}

function _pki_opt_filenames() {
    if _pki_bash4; then
        compopt -o filenames
    fi
}

function _pki_opt_nospace() {
    if _pki_bash4; then
        compopt -o nospace
    fi
}

function _pki_opt_nodefault() {
    if _pki_bash4; then
        compopt +o default
    fi
}

function _pki_append_space() {
    if ! _pki_bash3; then
        return
    elif [[ ${#COMPREPLY[@]} -ne 1 ]]; then
        return
    elif [[ $COMP_CWORD -lt ${#COMP_WORDS[@]}-1 ]]; then
        return
    elif [[ $1 == filenames && -d "${COMPREPLY[0]}" ]]; then
        return
    fi
    COMPREPLY[0]+=" "
}

function _pki_wordlist() {
    local list="$1"
    local cur="$2"

    _pki_opt_nodefault
    COMPREPLY=($(compgen -W "$list" -- "$cur"))
    _pki_append_space
}

function _pki_sections() {
    local suffix="$1"
    local cur="$2"

    _pki_opt_nodefault
    COMPREPLY=($(compgen -W "$(_pki_config_sections "$suffix")" -- "$cur"))
    _pki_append_space
}

function _pki_filenames() {
    local xspec="$1"
    local cur="$2"
    local IFS=$'\n'

    if [[ $BASH ]]; then
        COMPREPLY=($(compgen -f -X "$xspec" -- "$cur" | _pki_file_filter))
        if [[ ${#COMPREPLY[@]} -eq 0 ]]; then
            COMPREPLY=($(compgen -f -- "$cur" | _pki_file_filter))
        fi
        COMPREPLY+=($(compgen -d -- "$cur" | _pki_dir_filter))
        _pki_opt_filenames
        _pki_append_space filenames
    else
        COMPREPLY=()
    fi
}

if _pki_bash3; then
    complete -o default -o nospace -F _pki pki
else
    complete -o default -F _pki pki
fi


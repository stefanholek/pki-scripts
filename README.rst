PKI Tutorial Scripts
====================

Overview
--------

The **pki** utility is designed to make it easier to follow along with the
`PKI Tutorial`_ examples.
It provides a set of wrappers around OpenSSL commands with smart defaults and
TAB-completion of options and arguments.

.. _`PKI Tutorial`: https://pki-tutorial.readthedocs.io/en/latest/

Installation
------------

To install, first fetch the scripts from the repository::

    git clone https://bitbucket.org/stefanholek/pki-scripts

Then add this to your ~/.bashrc::

    alias pki=/path/to/pki-scripts/pki
    source /path/to/pki-scripts/pki-completion.bash

or this to your ~/.zshrc::

    autoload -U +X compinit && compinit
    autoload -U +X bashcompinit && bashcompinit

    alias pki=/path/to/pki-scripts/pki
    source /path/to/pki-scripts/pki-completion.bash

and open a new terminal window.

CA Commands
-----------

**pki db** -config filename
    Create a CA directory structure and initialize the CA database.

    The script will prompt before overwriting existing database files.

    ::

        pki db -config etc/root-ca.conf
        pki db -config etc/signing-ca.conf

**pki req** -config filename [-key filename] [-out filename] [-keyout filename] [-nodes] [-extensions name]
    Create a CSR and (optionally) a private key.

    If -config specifies a CA configuration file, both -out and -keyout are
    optional.
    If -out is given but -keyout is not, the key is placed next to the CSR.
    If -nodes is given the key will have no passphrase.
    If -key is given, the specified key is used instead of generating a new
    key.
    If -key is given but -out is not, the CSR is placed next to the key.
    The -extensions option specifies the extensions added to the CSR.
    The script will prompt before overwriting existing key files.

    ::

        pki req -config etc/root-ca.conf
        pki req -config etc/signing-ca.conf
        pki req -config etc/email.conf -out certs/fred.csr
        pki req -config etc/email.conf -key certs/fred.key

**pki sign** -config filename [-in filename] [-out filename] [-selfsign] [-policy name] [-extensions name] [-days days]
    Create a certificate.

    If -selfsign is given, both -in and -out are optional.
    If -in is given but -out is not, the certificate is placed next to the
    CSR.
    The -policy option sets the naming policy used.
    The -extensions option specifies the extensions added to the certificate.
    The -days option specifies the certificate lifetime in days.
    The option also accepts an enddate in [YY]YYMMDDHHMMSSZ format.
    On macOS you can also use time deltas supported by the date utility,
    e.g. 10y, 6m, 2w, 7d, 24H, 10M, or 30S.

    ::

        pki sign -config etc/root-ca.conf -selfsign -extensions root_ca -days 20y
        pki sign -config etc/root-ca.conf -in ca/signing-ca.csr -extensions signing_ca
        pki sign -config etc/signing-ca.conf -in certs/fred.csr -extensions email

**pki revoke** -config filename [-in filename] [-serial serial] [-reason reason] [-keycomp timestamp] [-cacomp timestamp]
    Revoke a certificate.

    Either -in or -serial must be given.
    The -reason option specifies an optional reason code, e.g. superseded.
    The -keycomp option sets reason to keyCompromise and adds the given
    compromise time.
    The -cacomp option sets reason to CACompromise and adds the given
    compromise time.
    The timestamp format is [YY]YYMMDDHHMMSSZ.

    ::

        pki revoke -config etc/signing-ca.conf -in certs/fred.crt
        pki revoke -config etc/signing-ca.conf \
            -serial 566D51764EF235038512DA7C8C714EF3978EF8E2 \
            -reason superseded
        pki revoke -config etc/signing-ca.conf \
            -serial 56:6d:51:76:4e:f2:35:03:85:12:da:7c:8c:71:4e:f3:97:8e:f8:e2 \
            -reason keyCompromise

**pki crl** -config filename [-out filename] [-extensions name] [-days days]
    Create a new CRL.

    If -out is omitted, the CRL is created in crl/<caname>.crl.
    The -extensions option specifies the extensions added to the CRL.
    The -days option sets the number of days until the next CRL.
    The option also accepts a date in [YY]YYMMDDHHMMSSZ format.
    On macOS you can also use time deltas supported by the date utility,
    e.g. 10y, 6m, 2w, 7d, 24H, 10M, or 30S.

    ::

        pki crl -config etc/signing-ca.conf

**pki expire** -config filename
    Scan the CA database for expired certificates and update their status
    fields.

    ::

        pki expire -config etc/signing-ca.conf

**pki query** -config filename [-fields list] [-all] [-noheader] [-E] [-i] [-n] [-v] [-w] [[-e] pattern ...]
    Query the CA database.

    The -fields option specifies a list of database fields to display. The
    default is 1,2,4,6.
    The -all option is a quick way to select all fields.
    If -noheader is given, the field names header is omitted from the output.
    The short options -E -i -n -v -w and -e are passed on to grep.

    ::

        pki query -config etc/signing-ca.conf
        pki query -config etc/signing-ca.conf -e Flintstone -w
        pki query -config etc/signing-ca.conf -e ^E -v
        pki query -config etc/signing-ca.conf -e ^R -fields 1-4,6

Key Commands
------------

**pki rsa** -out filename -bits keybits [-cipher alg]
    Create an RSA private key.

    The -bits option specifies the RSA key size.
    The -cipher option specifies the cipher algorithm used to encrypt the
    private key, e.g. des3 or aes256.
    If omitted the key will have no passphrase.
    The script will prompt before overwriting existing key files.

    ::

        pki rsa -bits 2048 -cipher aes256 -out certs/fred.key

**pki ec** -out filename [-paramfile filename] [-name curve] [-cipher alg]
    Create an EC private key.

    The -paramfile option specifies a parameter file (created with openssl
    ecparam.)
    The -name option selects the elliptic curve directly, e.g. P-256 or P-384.
    Either -paramfile or -name must be given.
    The -cipher option specifies the cipher algorithm used to encrypt the
    private key, e.g. des3 or aes256.
    If omitted the key will have no passphrase.
    The script will prompt before overwriting existing key files.

    ::

        pki ec -paramfile etc/ec.conf -cipher aes256 -out certs/fred.key
        pki ec -name P-256 -cipher aes256 -out certs/fred.key

Packaging Commands
------------------

**pki pkcs7** -out filename [-certfile filename ...]
    Create a PKCS#7 certificate bundle.

    The -certfile option may be specified more than once.

    ::

        pki pkcs7 -out certs/fred-email.p7c \
            -certfile certs/fred.crt \
            -certfile ca/signing-ca.crt \
            -certfile ca/root-ca.crt

**pki pkcs12** -in filename -out filename [-key filename] [-name name] [-certfile filename ...] [-caname name ...]
    Create a PKCS#12 key + certificate bundle.

    The -in option specifies the certificate to include.
    If -key is omitted, the private key must be included in the input file.
    If -name is omitted, a friendly name is derived from the certificate
    subject.
    The -certfile option may be specified more than once.
    The -caname option may be specified more than once.

    ::

        pki pkcs12 -out certs/fred-email.p12 \
            -in certs/fred.crt \
            -key certs/fred.key \
            -certfile ca/signing-ca.crt \
            -certfile ca/root-ca.crt

Display Commands
----------------

**pki view** [-long] [-debug] [-nokeys] [-type type] filename
    Display the contents of files in text form.

    The file type is determined by the file extension.
    If auto-detection does not produce the desired results, you can set the
    file type with the -type option.
    Supported types are: key, req, x509, crl, pkcs7, pkcs12, and pem.
    If -long is given, issuer and subject DNs are printed in multiline format.
    If -debug is given, the fields' data types are printed as well.
    If -nokeys is given, key and signature data is omitted from the output.

    ::

        pki view certs/fred.key
        pki view certs/fred.csr
        pki view -long -nokeys certs/fred.crt
        pki view -type x509 certs/fred.pem

**pki asn1** [-indent] [-dump] filename
    Display the ASN.1 structure of files.

    If -indent is given, output is indented to show structure depth.
    If -dump is given, data fields are printed in hexdump format.

    ::

        pki asn1 certs/fred.key
        pki asn1 certs/fred.crt
        pki asn1 -indent certs/fred-email.p7c

**pki enddate** [-utctime] [-gentime] filename
    Display the expiration date of a certificate.

    The -utctime and -gentime options return the date in YYMMDDHHMMSSZ and
    YYYYMMDDHHMMSSZ format respectively.

    ::

        pki enddate certs/fred.crt
        pki enddate -gentime certs/fred.crt

**pki serial** filename
    Display the serial number of a certificate in uppercase hex format.
    This is the format used for CA database entries and for filenames in the
    certificate archive.

    ::

        pki serial certs/fred.crt

**pki hash** filename
    Display the hash value of a certificate.
    This is the value used for symlinks in CA cert directories.

    ::

        pki hash certs/fred.crt

Notes
-----

All options may be abbreviated, typically to their first character.
Exceptions are -keyout (-t), -caname (-a), -cacomp (-a), and
-noheader (-d).

The -help (-h) option displays help for pki and pki commands.
The -version (-v) option shows pki and OpenSSL version strings.

Options do not have to appear before arguments.

All input files are expected to be in PEM format, except for the view and asn1
commands which accept both PEM and DER input.

All commands produce PEM format output, except for the pkcs7 and pkcs12
commands which produce DER.

Names of extensions may be given without the _ext and _reqext suffixes.
Naming policies may be specified without the _pol suffix.

Bash completion allows you to TAB-complete pki commands and their
options and arguments.

Set the OPENSSL environment variable to override the openssl binary used by
scripts.


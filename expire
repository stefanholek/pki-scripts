#!/bin/bash
#
# This script maintains the CA database
#
SCRIPT="pki $(basename "$0")"
SCRIPTDIR=$(dirname "$0")
CONFIG=""

source "$SCRIPTDIR/libpki.sh"

function usage() {
    err_exit "Usage: $SCRIPT -config filename"
}

function parse_args() {
    local arg=""
    for arg in "$@"; do
        if [ "$CONFIG" = "$MARKER" ]; then
            assign CONFIG "$arg"
        elif [ "$arg" = "-c" -o "$arg" = "-config" ]; then
            expect CONFIG
        elif [ "$arg" = "-printopts" ]; then
            msg_exit "-config -help"
        else
            usage
        fi
    done
    for arg in CONFIG; do
        if [ "${!arg}" = "$MARKER" ]; then usage; fi
    done
    if [ ! "$CONFIG" ]; then
        err_exit "$SCRIPT: -config option required"
    fi
}

# Main
parse_args "$@"
check_read "$CONFIG"

CA=$(config_value "$CONFIG" default ca)
DIR=$(config_value "$CONFIG" default dir)

if [ ! "$CA" -o ! "$DIR" ]; then
    err_exit "$SCRIPT: $CONFIG: Bad configuration file"
fi

if [ ! -d "$DIR/ca/$CA/private" ]; then
    err_exit "$SCRIPT: $DIR: No CA structure"
fi

$openssl ca -updatedb \
    -config "$CONFIG"


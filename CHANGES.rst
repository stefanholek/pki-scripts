Changes
=======

2022.8 - Unreleased
-------------------

- pkcs7, pkcs12: Fix mktemp templates for Linux
- pkcs7, pkcs12: Run certfiles through pem_filter
- pkcs12: Fix friendly name generation
- query: Change -noheader abbreviation to -d
- view: Use openssl pkey to view keys
- asn1: Make output indentation optional
- revoke: Allow UTCTime format with -keycomp and -cacomp
- revoke: Unspecified reason means no extension
- completion: Return all files if xspec does not produce matches
- completion: Remove delta-CRL reasons from completion
- completion: Make TAB-completion work in Zsh
- completion: Fix completion when pki is not aliased
- req: Independent defaults for -out and -keyout
- req: Let -keyout imply -out for symmetry
- pki: Replace platform specific find -s with sort
